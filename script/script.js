
function getFactorial (n){
if (n != 1) {
    return (n * getFactorial(n - 1));
} else { 
    return 1; }
}
n = +prompt("Enter number", '');
while (!n || isNaN(n)) { n = +prompt("Enter number", n); }
alert(`Factorial of number ${n} : ${getFactorial(n)}`);